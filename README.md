MIT License

Copyright (c) [2022] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****Note*****
I selected the MIT License because I think it has the most flexibility and freedoms
as far as allowing the use and modification of the program. I have looked into, and
will further research, the GNU GPLv3. I am interested in it's claim to protect code
from being restricted to the public by governments and corporations, but to fully
understand the agreement I will have to take a deeper look into it.


*****To build and install the program:

1.	Create new project in Visual Studios.  
2.	Open Shapes folder.
3.	Copy uncommented text from PAGE 01 and paste it into the Program.cs page.
4.	Create new class called Shapes.cs.
5.	Copy uncommented text from PAGE 02 and paste it into the Shapes.cs page.

The program is runnable at this point.

*****To operate the program:

1.	Menu appears on load.
2.	Follow prompt to select a shape or exit the program.
3.	Follow prompts to enter relevant information about the dimensions of the shape.
4.	Obtain output.
5.	Follow prompts to enter new dimensions or return to menu.

*****Also available from wiki link:

A Capstone Customer might want to use a private repository as they would be working with
many outside clients.  This allows the customer to distribute specific documents exclusively
to members of the project’s development team, instead of companywide.  They would also be
able to see the project being built, modified, and eventually completed.  This make it
easier for the customer to be involved and available for input.